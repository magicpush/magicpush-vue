const host = 'https://app.getmagicpush.com';

function injectCSS(css) {
    let el = document.createElement('style');
    el.type = 'text/css';
    el.innerText = css;
    document.head.appendChild(el);
    return el;
}

function setAttributes(e, attrs) {
    for(const a in attrs) {
        e.setAttribute(a, attrs[a]);
    }
}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');
    try {
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    } catch (e) {
        console.error("Failed to convert base64 string to Uint8Array:", e);
        return null;
    }
}

function isIOS() {
    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}

function isRunningStandalone() {
    if (window.parent.navigator.standalone) {
        return true; // iOS Safari
    } else if (window.parent.matchMedia('(display-mode: standalone)').matches) {
        return true; // Android and others
    }
    return false;
}

class MagicPushPlugin {
    constructor(appId, swUrl, { showButton = true, autoShowModal = false } = {}) {
        this.appId = appId;
        this.swUrl = swUrl;
        this.showButton = showButton;
        this.autoShowModal = autoShowModal;
    }

    init() {
        this.addCss();
        this.createButton();
        this.createModal();
        this.registerServiceWorker();
    }

    addCss() {
        injectCSS("@-webkit-keyframes pop {" +
            "  0% {" +
            "    transform: scale(1);" +
            "  }" +
            "  50% {" +
            "    transform: scale(0.9);" +
            "  }" +
            "  100% {" +
            "    transform: scale(.8);" +
            "  }" +
            "}" +
            "@keyframes pop {" +
            "  0% {" +
            "    transform: scale(1);" +
            "  }" +
            "  50% {" +
            "    transform: scale(0.9);" +
            "  }" +
            "  100% {" +
            "    transform: scale(.8);" +
            "  }" +
            "}" +
            ".MagicPushButton:active { " +
            "  -webkit-animation: pop 0.2s;" +
            "  animation: pop 0.2s;" +
            "}" +
            ".MagicPushButton:active:hover { " +
            "   transform: scale(.8);" +
            "}" +
            ".MagicPushModal.active {" +
            "   display: block;" +
            "   transform: scale(1)!important; " +
            "}" +
            "@media only screen and (max-width: 768px) {" +
            "   .MagicPushModal { width: 100%!important; right: 0!important; bottom: 0!important; " +
            "       border-bottom-left-radius: 0px!important; border-bottom-right-radius: 0px!important; " +
            "       transform-origin: bottom center!important; transition: all .2s ease-in-out!important; " +
            "       transform: translateY(100%)!important;" +
            "       -webkit-box-shadow: 0px -2px 8px 0px rgba(0,0,0,0.1)!important;" +
            "       -moz-box-shadow: 0px -2px 8px 0px rgba(0,0,0,0.1)!important;" +
            "       box-shadow: 0px -2px 8px 0px rgba(0,0,0,0.1)!important; }" +
            "   .MagicPushModal.active { " +
            "       transform: translateY(1%)!important; " +
            "   }" +
            "}"
        );
    }

    createButton() {
        this.button = document.createElement('div');
        this.buttonIframe = document.createElement('iframe');

        setAttributes(this.button, {
            'id': 'MagicPushButton',
            'class': 'MagicPushButton',
            'style': 'width: 60px;' +
                'height: 60px;' +
                'position: fixed;' +
                'bottom: 25px;' +
                'right: 25px;' +
                'border-radius: 15px;' +
                'box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);'
        });

        setAttributes(this.buttonIframe, {
            'src': host + '/lib/button/' + this.appId,
            'id': 'ButtonIframe',
            'style': 'width: 100%; height: 100%; border: none; overflow: hidden;'
        });

        if (this.showButton) {
            document.body.appendChild(this.button);
            this.button.appendChild(this.buttonIframe);
        }
    }

    createModal() {
        this.modal = document.createElement('div');
        this.modalIframe = document.createElement('iframe');

        setAttributes(this.modal, {
            'id': 'MagicPushModal',
            'class': 'MagicPushModal',
            'style': 'position: fixed; ' +
                'right: 25px; ' +
                'bottom: 100px; ' +
                'width: 400px; ' +
                'overflow: hidden; ' +
                'transform: scale(0); ' +
                'transform-origin: bottom right; ' +
                'transition: transform 0.2s ease-in-out;' +
                'border-radius: 0.8rem;' +
                'box-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);'
        });

        setAttributes(this.modalIframe, {
            'src': host + '/lib/modal/' + this.appId,
            'id': 'ModalIframe',
            'style': 'width: 100%; height: 100%; border: none;'
        });

        document.body.appendChild(this.modal);
        this.modal.appendChild(this.modalIframe);
    }

    registerServiceWorker() {
        let self = this;
        navigator.serviceWorker.register(self.swUrl, { scope: self.swUrl })
            .then((registration) => {
                window.addEventListener('message', function (event) {
                    console.log(event.data);
                    if (event.data.message === 'button-clicked') {
                        // Handle button click here, e.g., toggle modal visibility
                        self.toggleModal();
                    }
                    if (event.data.message === 'modal-resize') {
                        self.modal.style.height = event.data.height + 1 + "px";
                    }
                    if (event.data.message === 'created-app-user-id') {
                        localStorage.setItem('magicpush_app_user_id', event.data.appUserId);
                    }
                    if (event.data.message === 'accept-push') {
                        self.toggleModal();

                        Notification.requestPermission().then(function(result) {
                            if (result === 'granted') {
                                // alert(registration.toString());
                                console.log('Service worker successfully registered.');
                                if (registration.pushManager) {
                                    registration.pushManager.subscribe({ userVisibleOnly: true, applicationServerKey: urlBase64ToUint8Array(event.data.publicKey) })
                                        .then(function(subscription) {
                                            self.modalIframe.contentWindow.postMessage({ message: 'push-subscribed', subscription: subscription.toJSON() }, '*')
                                            window.postMessage({ message: 'push-subscribed', subscription: subscription.toJSON() }, '*')
                                        })
                                        .catch(function(error) {
                                            alert('Error subscribing to push notifications:' + error);
                                        });
                                } else {
                                    alert('Push notifications are not supported in your browser. If you are on Safari please update to the latest version.');
                                }
                            } else {
                                alert('You have denied notifications. You can change this in your browser settings.')
                            }
                        });
                    }

                    if (event.data.message === 'unsubscribe-push') {
                        registration.pushManager.getSubscription()
                            .then(function (subscription) {
                                if (subscription) {
                                    subscription.unsubscribe()
                                        .then(function (successful) {
                                            if (successful) {
                                                self.modalIframe.contentWindow.postMessage({message: 'push-unsubscribed'}, '*')
                                                window.postMessage({message: 'push-unsubscribed'}, '*')
                                            }
                                        })
                                        .catch(function (error) {
                                            console.log('Error unsubscribing from push:', error);
                                        });
                                }
                            })
                            .catch(function (error) {
                                console.log('Error getting push subscription:', error);
                            });
                    }
                }, false);

                self.modalIframe.onload = function() {
                    let appUserId = localStorage.getItem('magicpush_app_user_id');
                    console.log('appUserId: ' + appUserId);
                    if (appUserId != null ) {
                        self.modalIframe.contentWindow.postMessage({ message: 'app-user-id', appUserId: appUserId }, '*');
                    } else {
                        self.modalIframe.contentWindow.postMessage({ message: 'new-app-user-id' }, '*');
                    }

                    let isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
                    self.modalIframe.contentWindow.postMessage({ message: 'is-safari', isSafari: isSafari }, '*');
                    self.modalIframe.contentWindow.postMessage({ message: 'is-ios', isIOS: isIOS() }, '*');
                    self.modalIframe.contentWindow.postMessage({ message: 'is-running-standalone', isRunningStandalone: isRunningStandalone() }, '*');

                    if (registration.pushManager) {
                        registration.pushManager.getSubscription()
                            .then(function (subscription) {
                                if (subscription) {
                                    self.modalIframe.contentWindow.postMessage({
                                        message: 'has-subscription',
                                        subscription: subscription.toJSON()
                                    }, '*')
                                } else {
                                    if (self.autoShowModal) {
                                        self.toggleModal();
                                    }
                                }
                            })
                            .catch(function (error) {
                                console.log('Error getting push subscription:', error);
                            });
                    }
                }
            });
    }

    toggleModal() {
        this.button.classList.toggle('active');
        this.modal.classList.toggle('active');
    }

    listener(callback) {
        window.addEventListener('message', (event) => {
            if (event.data.message === 'push-subscribed') {
                callback({
                    type: 'push-subscribed',
                    user_id: localStorage.getItem('magicpush_app_user_id'),
                });
            } else if (event.data.message === 'push-unsubscribed') {
                callback({
                    type: 'push-unsubscribed',
                    user_id: localStorage.getItem('magicpush_app_user_id'),
                });
            }
        }, false);
    }
}

const MagicPushVuePlugin = {
    install(app, options) {
        app.config.globalProperties.$magicpush = {
            initialize({ appId, swUrl, showButton = true, autoShowModal = false }) {
                this.magicPushPlugin = new MagicPushPlugin(appId, swUrl, { showButton, autoShowModal });
                this.magicPushPlugin.init();
            },
            toggleModal() {
                this.magicPushPlugin.toggleModal();
            },
            addListener(callback) {
                this.magicPushPlugin.listener(callback);
            }
        };
    }
}

export default MagicPushVuePlugin;
